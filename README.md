# Overview
[![pipeline status](https://gitlab.com/volantmq/vlplugin/debug/badges/master/pipeline.svg)](https://gitlab.com/volantmq/vlplugin/debug/commits/master)

## Config
Docker image of VolantMQ service comes with `debug` plugin
To enable plugin add `debug` value to list of enabled plugins. In config section listening port and path can be specified
```yaml
plugins:
  enabled:
    - debug
  config:
    debug:
      - backend: prof.profiler
        config:
          port: 8080         # listener port. if omitted then default one from server is used
          path: /debug/pprof # path profiler is available. default /debug/pprof 
          cmdline: true      # report command line arguments server runs with. default false
          symbol: true       # default false
          cpu: true          # pprof-formatted cpu profile. default false 
          trace: true        # default false
          block: 100000      # block profile rate. default 0
          mutex: 1000000     # mutex profile rate. default 0
```
